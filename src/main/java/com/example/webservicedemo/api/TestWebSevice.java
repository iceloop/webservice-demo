package com.example.webservicedemo.api;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;

import javax.xml.namespace.QName;

/**
 * Controller
 *
 * @author hrniu
 * @Description:
 * @date 2021/7/20
 */
public class TestWebSevice {
    public static void main(String[] args) {
        //TestWebSevice.mainfactory();
        TestWebSevice.maincjlibe();
    }


    /**
     * 1.代理类工厂的方式,需要拿到对方的接口地址
     */
    public static void mainfactory() {
        try {
            // 接口地址
            String address = "http://localhost:8080/services/jobListService?wsdl";
            // 代理工厂
            JaxWsProxyFactoryBean jaxWsProxyFactoryBean = new JaxWsProxyFactoryBean();
            // 设置代理地址
            jaxWsProxyFactoryBean.setAddress(address);
            // 设置接口类型
            jaxWsProxyFactoryBean.setServiceClass(JobListService.class);
            // 创建一个代理接口实现
            JobListService us = (JobListService) jaxWsProxyFactoryBean.create();
            // 数据准备
            String userId = "zz";
            // 调用代理接口的方法调用并返回结果
            String result = us.getList("", 1);
            System.out.println("返回结果:" + result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 2：动态调用
     */
    public static void maincjlibe() {
        // 创建动态客户端
        JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();
        Client client = dcf.createClient("http://localhost:8080/services/jobListService?wsdl");
        // 需要密码的情况需要加上用户名和密码
        // client.getOutInterceptors().add(new ClientLoginInterceptor(USER_NAME, PASS_WORD));
        Object[] objects = new Object[0];
        try {
            //如果有命名空间需要加上这个，第一个参数为命名空间名称，第二个参数为WebService方法名称
            QName operationName = new QName("http://api.webservicedemo.example.com/","getList");
            // invoke("方法名",参数1,参数2,参数3....);
            objects = client.invoke(operationName, "maple",2);
            System.out.println("返回数据:" + objects[0]);
        } catch (java.lang.Exception e) {
            e.printStackTrace();
        }
    }


}
