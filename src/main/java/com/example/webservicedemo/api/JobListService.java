package com.example.webservicedemo.api;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface JobListService {

    @WebMethod
    String getList(@WebParam(name = "userId") String userId, @WebParam(name = "agentNum") Integer agentNum);
}